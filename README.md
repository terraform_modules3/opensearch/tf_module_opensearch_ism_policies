# Terraform Opensearch ISM Policies Module
Creates Opensearch ism policies

## Usage example
```hcl
# main.tf
module "ism_policy" {
  source = "git::https://gitlab.com/terraform_modules3/opensearch/tf_module_opensearch_ism_policies.git?ref=0.1.0"
}
```
```
.
├── main.tf
└── ism_policies
    └── logs_default.json
```
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.1 |
| <a name="requirement_opensearch"></a> [opensearch](#requirement\_opensearch) | >= 2.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_opensearch"></a> [opensearch](#provider\_opensearch) | 2.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [opensearch_ism_policy.policy](https://registry.terraform.io/providers/opensearch-project/opensearch/latest/docs/resources/ism_policy) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ism_policies_dir"></a> [ism\_policies\_dir](#input\_ism\_policies\_dir) | Path to dir containing component\_tempates definintions in .json files<br><br>  Example dir structure:<br>  ism\_policies/<br>  └── logs\_default.json<br><br>  Name of a file is used as a name of component\_tempate | `string` | `"./ism_policies"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
