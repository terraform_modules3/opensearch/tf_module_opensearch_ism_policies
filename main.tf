locals {
  files = fileset(var.ism_policies_dir, "*.json")
}

resource "opensearch_ism_policy" "policy" {
  for_each = { for file in local.files : trimsuffix(file, ".json") => file }

  policy_id = each.key
  body      = file("${var.ism_policies_dir}/${each.value}")
}
