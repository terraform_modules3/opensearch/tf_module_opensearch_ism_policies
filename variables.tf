variable "ism_policies_dir" {
  description = <<EOT
  Path to dir containing component_tempates definintions in .json files

  Example dir structure:
  ism_policies/
  └── logs_default.json

  Name of a file is used as a name of component_tempate

  EOT
  type        = string
  default     = "./ism_policies"
}
